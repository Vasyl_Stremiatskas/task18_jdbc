package com.stremiatskas.service;

import com.stremiatskas.DAO.implementation.DishStoreDAOImpl;
import com.stremiatskas.model.FK_DishStore;

import java.sql.SQLException;
import java.util.List;

public class DishStoreService {
    public List<FK_DishStore> findAll() throws SQLException {
        return new DishStoreDAOImpl().findAll();
    }


    public int create(FK_DishStore entity) throws SQLException{
        return new DishStoreDAOImpl().create(entity);
    }

    public int update(FK_DishStore entity) throws SQLException{
        return new DishStoreDAOImpl().update(entity);
    }

    public int delete(FK_DishStore s) throws SQLException{
        return new DishStoreDAOImpl().delete(s);
    }

}
