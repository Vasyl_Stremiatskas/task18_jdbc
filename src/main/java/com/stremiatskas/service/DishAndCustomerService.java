package com.stremiatskas.service;

import com.stremiatskas.DAO.implementation.DishAndCustumerDAOImpl;
import com.stremiatskas.model.FK_DishAndCustomer;

import java.sql.SQLException;
import java.util.List;

public class DishAndCustomerService {
    public List<FK_DishAndCustomer> findAll() throws SQLException {
        return new DishAndCustumerDAOImpl().findAll();
    }


    public int create(FK_DishAndCustomer entity) throws SQLException{
        return new DishAndCustumerDAOImpl().create(entity);
    }

    public int update(FK_DishAndCustomer entity) throws SQLException{
        return new DishAndCustumerDAOImpl().update(entity);
    }

    public int delete(FK_DishAndCustomer s) throws SQLException{
        return new DishAndCustumerDAOImpl().delete(s);
    }

}
