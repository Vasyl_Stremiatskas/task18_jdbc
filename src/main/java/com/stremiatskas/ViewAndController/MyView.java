package com.stremiatskas.ViewAndController;

import com.stremiatskas.model.CustumerTableModel;
import com.stremiatskas.model.DishModel;
import com.stremiatskas.model.ProductModel;
import com.stremiatskas.model.WaiterModel;
import com.stremiatskas.model.metadata.TableMetaData;
import com.stremiatskas.persistant.ConnectionManager;
import com.stremiatskas.service.*;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Time;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("A", "   A - Select all table");
        menu.put("B", "   B - Select structure of DB");

        menu.put("1", "   1 - Table: Dish");
        menu.put("11", "  11 - Create for Dish");
        menu.put("12", "  12 - Update Dish");
        menu.put("13", "  13 - Delete from Dish");
        menu.put("14", "  14 - Select Dish");
        menu.put("15", "  15 - Find Dish by ID");
//        menu.put("16", "  16 - Delete from Department and move all employees to another department");

        menu.put("2", "   2 - Table: Custumer_table");
        menu.put("21", "  21 - Create for Custumer_table");
        menu.put("22", "  22 - Update Custumer_table");
        menu.put("23", "  23 - Delete from Custumer_table");
        menu.put("24", "  24 - Select Custumer_table");
        menu.put("25", "  25 - Find Custumer_table by ID");


        menu.put("3", "   3 - Table: Product");
        menu.put("31", "  31 - Create for Product");
        menu.put("32", "  32 - Update Product");
        menu.put("33", "  33 - Delete from Product");
        menu.put("34", "  34 - Select Product");
        menu.put("35", "  35 - Find Product by ID");

        menu.put("4", "   4 - Table: Waiter");
        menu.put("41", "  41 - Create for Waiter");
        menu.put("42", "  42 - Update Waiter");
        menu.put("43", "  43 - Delete from Waiter");
        menu.put("44", "  44 - Select Waiter");
        menu.put("45", "  45 - Find Waiter by ID");

        menu.put("Q", "   Q - exit");

        methodsMenu.put("A", this::selectAllTable);
        methodsMenu.put("B", this::takeStructureOfDB);

        methodsMenu.put("11", this::createForDish);
        methodsMenu.put("12", this::updateDish);
        methodsMenu.put("13", this::deleteFromDish);
        methodsMenu.put("14", this::selectDish);
        methodsMenu.put("15", this::findDishByID);


        methodsMenu.put("21", this::createForCustumerTable);
        methodsMenu.put("22", this::updateCustumerTable);
        methodsMenu.put("23", this::deleteFromCustumerTable);
        methodsMenu.put("24", this::selectCustumerTable);
        methodsMenu.put("25", this::findCustumerTableByID);


        methodsMenu.put("31", this::createForProduct);
        methodsMenu.put("32", this::updateProduct);
        methodsMenu.put("33", this::deleteFromProduct);
        methodsMenu.put("34", this::selectProduct);
        methodsMenu.put("35", this::findProductByID);

        methodsMenu.put("41", this::createForDWaiter);
        methodsMenu.put("42", this::updateWaiter);
        methodsMenu.put("43", this::deleteFromWaiter);
        methodsMenu.put("44", this::selectWaiter);
        methodsMenu.put("45", this::deleteFromWaiterMoveCustomerTable);
    }

    private void selectAllTable() throws SQLException {
        selectDish();
        selectCustumerTable();
        selectProduct();
        selectWaiter();
    }

    private void takeStructureOfDB() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        MetaDataService metaDataService = new MetaDataService();
        List<TableMetaData> tables = metaDataService.getTablesStructure();
        System.out.println("TABLE OF DATABASE: "+connection.getCatalog());

        for (TableMetaData table: tables ) {
            System.out.println(table);
        }
    }


    private void deleteFromDish() throws SQLException {
        System.out.println("Input ID(dish_name) for Dish: ");
        String id = input.nextLine();
        DishService departmentService = new DishService();
        int count = departmentService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForDish() throws SQLException {
        System.out.println("Input ID(dish_name)) for Dish: ");
        String dishName = input.nextLine();
        System.out.println("Input price for Dish: ");
        BigDecimal price = input.nextBigDecimal();
        System.out.println("Input type_in_menu for Dish: ");
        String typeInMenu = input.nextLine();
        DishModel entity = new DishModel(dishName, price, typeInMenu);

        DishService dishService = new DishService();
        int count = dishService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateDish() throws SQLException {
        System.out.println("Input ID(dish_name)) for Dish: ");
        String dishName = input.nextLine();
        System.out.println("Input price for Dish: ");
        BigDecimal price = input.nextBigDecimal();
        System.out.println("Input type_in_menu for Dish: ");
        String typeInMenu = input.nextLine();
        DishModel entity = new DishModel(dishName, price, typeInMenu);

        DishService dishService = new DishService();
        int count = dishService.update(entity);
        System.out.printf("There are update %d rows\n", count);
    }

    private void selectDish() throws SQLException {
        System.out.println("\nTable: Dish");
        DishService dishService = new DishService();
        List<DishModel> dish = dishService.findAll();
        for (DishModel entity : dish) {
            System.out.println(entity);
        }
    }

    private void findDishByID() throws SQLException {
        System.out.println("Input ID(dish_name) for Dish: ");
        String dish_name = input.nextLine();
        DishService dishService = new DishService();
        DishModel dishID = dishService.findById(dish_name);
        System.out.println(dishID);
    }



    //------------------------------------------------------------------------

    private void deleteFromCustumerTable() throws SQLException {
        System.out.println("Input ID(id_table) for Custumer_table: ");
        Integer id = input.nextInt();
        input.nextLine();
        CustumerTableService tableService = new CustumerTableService();
        int count = tableService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForCustumerTable() throws SQLException {
        System.out.println("Input ID(id_table) for Custumer_table: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.println("Input ordertime for Custumer_table: ");
        Time ordertime = Time.valueOf(input.nextLine());
        System.out.println("Input id_waiter for Custmer_table: ");
        Integer idWaiter = input.nextInt();
        CustumerTableModel entity = new CustumerTableModel(id,ordertime,idWaiter);
        CustumerTableService tableService = new CustumerTableService();

        int count = tableService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateCustumerTable() throws SQLException {
        System.out.println("Input ID(id_table) for Custumer_table: ");
        Integer id = input.nextInt();
        input.nextLine();
        System.out.println("Input ordertime for Custumer_table: ");
        Time ordertime = Time.valueOf(input.nextLine());
        System.out.println("Input id_waiter for Custmer_table: ");
        Integer idWaiter = input.nextInt();
        CustumerTableModel entity = new CustumerTableModel(id,ordertime,idWaiter);
        CustumerTableService tableService = new CustumerTableService();

        int count = tableService.update(entity);
        System.out.printf("There are update %d rows\n", count);
    }

    private void selectCustumerTable() throws SQLException {
        System.out.println("\nTable: Custumer_table");
        CustumerTableService tableService = new CustumerTableService();
        List<CustumerTableModel> tables = tableService.findAll();
        for (CustumerTableModel entity : tables) {
            System.out.println(entity);
        }
    }

    private void findCustumerTableByID() throws SQLException {
        System.out.println("Input ID(id_table) for Custumer_table: ");
        Integer id = input.nextInt();
        input.nextLine();
        CustumerTableService tableService = new CustumerTableService();
        CustumerTableModel tables = tableService.findById(id);
        System.out.println(tables);
    }



    //------------------------------------------------------------------------

    private void updateProduct() throws SQLException {
        System.out.println("Input ID(prod_name) for Product: ");
        String id = input.nextLine();
        ProductModel entity = new ProductModel(id);
        ProductService productService = new  ProductService();
        int count = productService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void deleteFromProduct() throws SQLException {
        System.out.println("Input ID(prod_name) for Product: ");
        String id = input.nextLine();
        ProductService productService = new  ProductService();
        int count = productService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForProduct() throws SQLException {
        System.out.println("Input ID(prod_name) for Product: ");
        String id = input.nextLine();
        ProductModel entity = new ProductModel(id);
        ProductService productService = new  ProductService();
        int count = productService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void selectProduct() throws SQLException {
        System.out.println("\nTable: Product");
        ProductService productService = new  ProductService();
        List<ProductModel> products = productService.findAll();
        for (ProductModel entity : products) {
            System.out.println(entity);
        }
    }

    private void findProductByID() throws SQLException {
        System.out.println("Input ID(prod_name) for Product: ");
        String id = input.nextLine();
        ProductService productService = new ProductService();
        ProductModel products = productService.findById(id);
        System.out.println(products);
    }


    //------------------------------------------------------------------------

    private void deleteFromWaiter() throws SQLException {
        System.out.println("Input ID(id_waiter) for Waiter: ");
        Integer id = input.nextInt();
        WaiterService waiterService = new WaiterService();
        int count = waiterService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void createForDWaiter() throws SQLException {
        System.out.println("Input ID(id_waiter) for Waiter: ");
        Integer id = input.nextInt();
        System.out.println("Input name for Waiter: ");
        String waiterName = input.nextLine();
        System.out.println("Input lastname for Waiter: ");
        String lastName = input.nextLine();
        WaiterModel entity = new WaiterModel(id, waiterName, lastName);

        WaiterService waiterService = new WaiterService();
        int count = waiterService.create(entity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateWaiter() throws SQLException {
        System.out.println("Input ID(id_waiter) for Waiter: ");
        Integer id = input.nextInt();
        System.out.println("Input name for Waiter: ");
        String waiterName = input.nextLine();
        System.out.println("Input lastname for Waiter: ");
        String lastName = input.nextLine();
        WaiterModel entity = new WaiterModel(id, waiterName, lastName);

        WaiterService waiterService = new WaiterService();
        int count = waiterService.update(entity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void selectWaiter() throws SQLException {
        System.out.println("\nTable: Waiter");
        WaiterService waiterService = new WaiterService();
        List<WaiterModel> waiters = waiterService.findAll();
        for (WaiterModel entity : waiters) {
            System.out.println(entity);
        }
    }

    private void findWaiterByID() throws SQLException {
        System.out.println("Input ID(id_waiter) for Waiter: ");
        Integer id = input.nextInt();
        WaiterService waiterService = new WaiterService();
        WaiterModel waiters = waiterService.findById(id);
        System.out.println(waiters);
    }

    private void deleteFromWaiterMoveCustomerTable() throws SQLException {
        System.out.println("Input ID(id_waiter) for Waiter: ");
        Integer idDeleted = input.nextInt();
        System.out.println("Input ID(id_waiter) for another Waiter (for move custumer_tables):");
        Integer idMoveTo = input.nextInt();

        WaiterService waiterService = new WaiterService();
        int count = waiterService.deleteWithMoveOfCustumerTable(idDeleted,idMoveTo);
        System.out.printf("There are deleted %d rows\n", count);
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet())
            if (key.length() == 1) System.out.println(menu.get(key));
    }

    private void outputSubMenu(String fig) {

        System.out.println("\nSubMENU:");
        for (String key : menu.keySet())
            if (key.length() != 1 && key.substring(0, 1).equals(fig)) System.out.println(menu.get(key));
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();

            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                System.out.println("Please, select menu point.");
                keyMenu = input.nextLine().toUpperCase();
            }

            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
