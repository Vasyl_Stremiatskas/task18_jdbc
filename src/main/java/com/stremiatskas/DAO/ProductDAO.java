package com.stremiatskas.DAO;

import com.stremiatskas.model.ProductModel;

public interface ProductDAO extends GeneralDAO<ProductModel, String> {
}
