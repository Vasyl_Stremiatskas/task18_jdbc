package com.stremiatskas.DAO;

import com.stremiatskas.model.WaiterModel;

public interface WaiterDAO extends GeneralDAO<WaiterModel, Integer> {
}
