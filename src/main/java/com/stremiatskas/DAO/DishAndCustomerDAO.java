package com.stremiatskas.DAO;

import com.stremiatskas.model.FK_DishAndCustomer;

public interface DishAndCustomerDAO extends GeneralDAO<FK_DishAndCustomer, FK_DishAndCustomer> {
}
