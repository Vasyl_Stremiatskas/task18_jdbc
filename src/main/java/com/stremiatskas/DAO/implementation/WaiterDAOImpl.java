package com.stremiatskas.DAO.implementation;

import com.stremiatskas.DAO.WaiterDAO;
import com.stremiatskas.model.WaiterModel;
import com.stremiatskas.persistant.ConnectionManager;
import com.stremiatskas.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WaiterDAOImpl implements WaiterDAO {

    private static final String FIND_ALL = "SELECT * FROM waiter";
    private static final String FIND_BY_ID="sELECT * FROM waiter WHERE id_waiter=?";
    private static final String CREATE = "INSERT waiter (id_waiter, name, lastname) values(?,?,?)";
    private static final String UPDATE ="UPDATE waiter SET name = ?, lastname=? WHERE id_waiter=?";
    private static final String DELETE ="DELETE FROM waiter WHERE id_waiter=?";

    @Override
    public List<WaiterModel> findAll() throws SQLException {
        List<WaiterModel> waiter = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(Statement statement= connection.createStatement()){
            try(ResultSet rs = statement.executeQuery(FIND_ALL)){
                while(rs.next())
                {
                    waiter.add((WaiterModel) new Transformer<>(WaiterModel.class).fromResultSetModel(rs));
                }
            }
        }
        return waiter;
    }

    @Override
    public WaiterModel findById(Integer integer) throws SQLException {
       WaiterModel model = null;
       Connection connection = ConnectionManager.getConnection();
       try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)){
           ps.setInt(1,integer);
           try (ResultSet rs = ps.executeQuery()){
               while(rs.next()) {
                   model = (WaiterModel) new Transformer<>(WaiterModel.class).fromResultSetModel(rs);
                   break;
               }
           }

       }
       return model;
    }

    @Override
    public int create(WaiterModel entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)){
            ps.setInt(1,entity.getWaiterNumber());
            ps.setString(2,entity.getNameWaiter());
            ps.setString(3,entity.getLastNameWaiter());
            return ps.executeUpdate();

        }
    }

    @Override
    public int update(WaiterModel entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)){

            ps.setString(1,entity.getNameWaiter());
            ps.setString(2,entity.getLastNameWaiter());
            ps.setInt(3,entity.getWaiterNumber());
            return ps.executeUpdate();

        }
    }

    @Override
    public int delete(Integer integer) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)){

            ps.setInt(1,integer);
            return ps.executeUpdate();

        }
    }
}
