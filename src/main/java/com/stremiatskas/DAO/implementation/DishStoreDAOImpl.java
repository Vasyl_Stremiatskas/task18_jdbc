package com.stremiatskas.DAO.implementation;

import com.stremiatskas.DAO.DishStoreDAO;
import com.stremiatskas.model.FK_DishStore;
import com.stremiatskas.persistant.ConnectionManager;
import com.stremiatskas.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DishStoreDAOImpl implements DishStoreDAO {

    private static final String FIND_ALL = "SELECT * FROM dish_store";
    private static final String CREATE = "INSERT dish_store (dish_name, prod_name, number) values(?,?,?)";
    private static final String UPDATE ="UPDATE dish_store SET dish_name=?, prod_name=?, number=?";
    private static final String DELETE ="DELETE * FROM dish_store";

    @Override
    public List<FK_DishStore> findAll() throws SQLException {
        List<FK_DishStore> store = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(Statement statement = connection.createStatement()){
            try(ResultSet rs = statement.executeQuery(FIND_ALL)){
                while(rs.next()){
                    store.add((FK_DishStore) new Transformer<>(FK_DishStore.class).fromResultSetModel(rs));
                }
            }
        }
        return store;
    }

    @Override
    public FK_DishStore findById(FK_DishStore fk_dishStore) throws SQLException {
        return null;
    }

    @Override
    public int create(FK_DishStore entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(CREATE)){
            ps.setString(1,entity.getDishName());
            ps.setString(2,entity.getProductName());
            ps.setString(3, entity.getDishGram());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(FK_DishStore entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(UPDATE)){
            ps.setString(1,entity.getDishName());
            ps.setString(2,entity.getProductName());
            ps.setString(3, entity.getDishGram());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(FK_DishStore fk_dishStore) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(DELETE)){
            ps.setString(1,fk_dishStore.getDishName());
            ps.setString(2,fk_dishStore.getProductName());
            ps.setString(3, fk_dishStore.getDishGram());
            return ps.executeUpdate();
        }
    }
}
