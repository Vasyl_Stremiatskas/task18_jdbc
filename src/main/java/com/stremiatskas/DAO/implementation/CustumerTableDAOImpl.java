package com.stremiatskas.DAO.implementation;

import com.stremiatskas.DAO.CustumerTableDAO;
import com.stremiatskas.model.CustumerTableModel;
import com.stremiatskas.persistant.ConnectionManager;
import com.stremiatskas.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustumerTableDAOImpl implements CustumerTableDAO {

    private static final String FIND_BY_WAITER_NUM = "SELECT * FROM custumer_table WHERE id_waiter=?";
    private static final String FIND_ALL = "SELECT * FROM custumer_table";
    private static final String DELETE = "DELETE FROM custumer_table WHERE id_table=?";
    private static final String CREATE = "INSERT custumer_table (id_table, ordertime, id_waiter) VALUES (?, ?, ?)";
    private static final String UPDATE = "UPDATE custumer_table SET ordertime=?, id_waiter=? WHERE id_table=?";
    private static final String FIND_BY_ID = "SELECT * FROM custumer_table WHERE id_table=?";



    @Override
    public List<CustumerTableModel> findByWaiterNum(Integer waiterNum) throws SQLException {
        List<CustumerTableModel> waiter = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(FIND_BY_WAITER_NUM)){
            ps.setInt(1, waiterNum);
            try (ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    waiter.add((CustumerTableModel) new Transformer<>(CustumerTableModel.class).fromResultSetModel(rs));
                }
            }
        }
        return waiter;
    }

    @Override
    public List<CustumerTableModel> findAll() throws SQLException {
        List<CustumerTableModel> custumerTable = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(Statement statement = connection.createStatement()){
            try(ResultSet rs = statement.executeQuery(FIND_ALL)){
                while(rs.next()){
                    custumerTable.add((CustumerTableModel) new Transformer<>(CustumerTableModel.class).fromResultSetModel(rs));
                }
            }
        }
        return custumerTable;
    }

    @Override
    public CustumerTableModel findById(Integer integer) throws SQLException {
        CustumerTableModel model = null;
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)){
            ps.setInt(1,integer);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    model=(CustumerTableModel) new Transformer<>(CustumerTableModel.class).fromResultSetModel(rs);
                    break;
                }
            }
        }
        return model;
    }

    @Override
    public int create(CustumerTableModel entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(CREATE)){
            ps.setInt(1,entity.getTableNum());
            ps.setTime(2,entity.getOrderTime());
            ps.setInt(3,entity.getWaiterNum());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(CustumerTableModel entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(UPDATE)){

            ps.setTime(1,entity.getOrderTime());
            ps.setInt(2,entity.getWaiterNum());
            ps.setInt(3,entity.getTableNum());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer integer) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement ps = connection.prepareStatement(DELETE)){
            ps.setInt(1, integer);
            return  ps.executeUpdate();
        }
    }
}
