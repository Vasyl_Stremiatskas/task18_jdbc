package com.stremiatskas.DAO;

import com.stremiatskas.model.DishModel;
import com.stremiatskas.model.FK_DishStore;

public interface DishStoreDAO extends GeneralDAO<FK_DishStore, FK_DishStore> {
}
