package com.stremiatskas.DAO;

import com.stremiatskas.model.DishModel;

public interface DishDAO extends GeneralDAO<DishModel,String> {
}
