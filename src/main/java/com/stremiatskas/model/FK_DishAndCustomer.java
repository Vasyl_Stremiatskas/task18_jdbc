package com.stremiatskas.model;

import com.stremiatskas.model.Annotation.Column;
import com.stremiatskas.model.Annotation.Table;

@Table(name="dish_and_custmer")
public class FK_DishAndCustomer {
    @Column(name = "dish_name", length = 25)
    private String dishName;
    @Column(name = "id_table")
    private Integer tableNumber;
    @Column(name = "order_amount", length = 10)
    private String orderAmount;

    public FK_DishAndCustomer() {
    }

    public FK_DishAndCustomer(String dishName, Integer tableNumber, String orderAmount) {
        this.dishName = dishName;
        this.tableNumber = tableNumber;
        this.orderAmount = orderAmount;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public Integer getTableNum() {
        return tableNumber;
    }

    public void setTableNum(Integer tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getOrderAmount(){return orderAmount;}

    public void setOrderAmount(String orderAmount){this.orderAmount=orderAmount;}
}
