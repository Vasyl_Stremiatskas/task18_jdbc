package com.stremiatskas.model;

import com.stremiatskas.model.Annotation.Column;
import com.stremiatskas.model.Annotation.PrimaryKey;
import com.stremiatskas.model.Annotation.Table;

import java.sql.Time;

@Table(name="custumer_table")
public class CustumerTableModel {
    @PrimaryKey
    @Column(name="id_table", length=11)
    private Integer tableNum;
    @Column(name="ordertime", length = 15)
    private Time orderTime;
    @Column(name="id_waiter" , length = 11)
    private Integer waiterNum;

    public CustumerTableModel(){}

    public CustumerTableModel(Integer tableNum, Time orderTime, Integer waiterNum)
    {
        this.tableNum=tableNum;
        this.orderTime=orderTime;
        this.waiterNum=waiterNum;
    }

    public Integer getTableNum(){return tableNum;}

    public void setTableNum(Integer tableNum){this.tableNum=tableNum;}

    public Integer getWaiterNum(){return waiterNum;}

    public void setWaiterNum(Integer waiterNum){this.waiterNum=waiterNum;}

    public Time getOrderTime(){return orderTime;}

    public void setOrderTime(Time orderTime){this.orderTime=orderTime;}

    @Override
    public String toString()
    {
         return String.format("%-5d %-7tR  %-5d", tableNum , orderTime, waiterNum);
    }
}

