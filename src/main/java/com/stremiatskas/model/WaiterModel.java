package com.stremiatskas.model;

import com.stremiatskas.model.Annotation.Column;
import com.stremiatskas.model.Annotation.PrimaryKey;
import com.stremiatskas.model.Annotation.Table;

@Table(name="waiter")
public class WaiterModel {

    @Column(name="id_waiter", length = 11)
    private Integer waiterNumber;
    @Column(name="name",length = 10)
    private String nameWaiter;
    @Column(name="lastname", length = 10)
    private String lastNameWaiter;

    public WaiterModel(){}

    public WaiterModel(int waiterNumber, String nameWaiter, String lastNameWaiter){
        this.waiterNumber=waiterNumber;
        this.nameWaiter=nameWaiter;
        this.lastNameWaiter=lastNameWaiter;
    }

    public Integer getWaiterNumber(){return waiterNumber;}

    public void setWaiterNumber(Integer waiterNumber){this.waiterNumber=waiterNumber;}

    public String getNameWaiter(){return nameWaiter;}

    public void setNameWaiter(String nameWaiter) {this.nameWaiter = nameWaiter; }

    public String getLastNameWaiter(){return lastNameWaiter;}

    public void setLastNameWaiter(String lastNameWaiter){this.lastNameWaiter=lastNameWaiter;}

    @Override
    public String toString(){return String.format("%-5d %-7s  %-7s", waiterNumber, nameWaiter, lastNameWaiter);}
}
