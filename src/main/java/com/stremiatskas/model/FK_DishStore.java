package com.stremiatskas.model;

import com.stremiatskas.model.Annotation.Column;
import com.stremiatskas.model.Annotation.Table;

@Table(name="dish_store")
public class FK_DishStore {
    @Column(name="dish_name", length = 25)
    private String dishName;
    @Column(name="prod_name",length = 25)
    private String productName;
    @Column(name="number", length = 20)
    private String dishGram;

    public FK_DishStore(){}

    public FK_DishStore(String dishName, String productName, String dishGram){
        this.dishName=dishName;
        this.productName=productName;
        this.dishGram=dishGram;
    }

    public String getDishName(){return dishName;}

    public void setDishName(String dishName){this.dishName=dishName;}

    public String getProductName(){return productName;}

    public void setProductName(String productName){this.productName=productName;}

    public String getDishGram(){return dishGram;}

    public void setDishGram(String dishGram){this.dishGram=dishGram;}

    @Override
    public String toString(){return String.format("%-5s %-5s %-5s", dishName, productName,dishGram);}


}
